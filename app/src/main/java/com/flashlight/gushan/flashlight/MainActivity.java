package com.flashlight.gushan.flashlight;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    Global global;
    Button button;
    RelativeLayout relativeLayout;
    String newString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        relativeLayout = (RelativeLayout)findViewById(R.id.relLayMain);

        global = new Global();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            global.setRelativeLayout(Objects.requireNonNull(relativeLayout));
        }

        startService(new Intent(this, ShakeActivity.class));

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                newString = "";
            } else {
                if (extras.getString(Intent.EXTRA_TEXT) != null) {
                    newString = extras.getString(Intent.EXTRA_TEXT);
                    if (newString.equals("Switch Off")) {
                        button = (Button) findViewById(R.id.btn_switch);
                        global.changeBackground(true);
                        button.setText("Switch Off");
                    } else if (newString.equals("Switch On")) {
                        button = (Button) findViewById(R.id.btn_switch);
                        global.changeBackground(false);
                        button.setText("Switch On");
                    }
                }
            }
        }else {
            newString= (String) savedInstanceState.getSerializable(Intent.EXTRA_TEXT);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        startService(new Intent(this, ShakeActivity.class));
        Log.i("Flashlight_onStart", "Starting service");
    }

    public void toggle(View view) {
        Button button = (Button) findViewById(R.id.btn_switch);
        Log.i("Flashlight_onStart", "Starting toggle " + button.getText());

        if (button.getText().equals("Switch On")) {
            Log.i("test", button + "");
            button.setText("Switch Off");
            torchToggle("on");
        } else {
            button.setText("Switch On");
            torchToggle("off");
        }
    }

    public void torchToggle(String command) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            String cameraId;
            try {
                cameraId = cameraManager.getCameraIdList()[0];
                Log.i("Flashlight_onStart", "camera id " + cameraId);
                if (command.equals("on")) {
                    cameraManager.setTorchMode(cameraId, true);
                    global.changeBackground(true);
                } else {
                    cameraManager.setTorchMode(cameraId, false);
                    global.changeBackground(false);
                }
            } catch (CameraAccessException e) {
                Log.i("Flashlight_onStart", "exception " + e.getMessage());
            }
        }
    }

    public void goToSettings(View view) {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    @Override
    protected void onDestroy() {
        startService(new Intent(this, ShakeActivity.class));
        super.onDestroy();
        Log.i("Flashlight_onDestroy", "Stopping service");
    }
}
