package com.flashlight.gushan.flashlight;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Objects;

public class SettingsActivity extends AppCompatActivity {
    SeekBar seekBar;
    TextView textView;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        seekBar = (SeekBar)findViewById(R.id.skbThreshold);
        textView = (TextView) findViewById(R.id.tvThresholdValue);
        imageView = (ImageView)findViewById(R.id.ivShakeGif);
        Glide.with(getApplicationContext())
                .load(R.drawable.chopflashlight)
                .into(imageView);

        seekBarChange();

        String progress = Global.loadFile(getApplicationContext(), "settings.txt");
        if (progress != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                seekBar.setProgress((int) Float.parseFloat(Objects.requireNonNull(progress).replace("f", "")));
            }
            textView.setText(progress);
        } else {
            textView.setText("");
        }

    }

    public void seekBarChange() {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textView.setText(new StringBuilder().append(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void saveSettings(View view) {
        if(Global.saveFile(getApplicationContext(), "settings.txt", seekBar.getProgress() + "")) {
            Log.i("Flashlight_saveSettings", "File Saved Successfully");
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            Log.e("Flashlight_saveSettings", "Error while saving file");
        }
    }
}
