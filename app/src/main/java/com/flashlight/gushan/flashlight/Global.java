package com.flashlight.gushan.flashlight;

import android.content.Context;
import android.graphics.Color;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.util.Log;
import android.widget.RelativeLayout;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Objects;

public class Global {

    private RelativeLayout layout;

    public void setRelativeLayout(RelativeLayout relativeLayout) {
        layout = relativeLayout;
    }

    public void changeBackground(boolean isTorchOn) {
        if (isTorchOn) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Objects.requireNonNull(layout).setBackgroundColor(Color.WHITE);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Objects.requireNonNull(layout).setBackgroundColor(Color.parseColor("#0098cc"));
            }
        }
    }

    public static boolean saveFile(Context context, String fileName, String data) {
        try {
            FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            Writer writer = new OutputStreamWriter(fos);
            writer.write(data);
            writer.close();
            Log.i("saveFile", "File saved successfully");
            return true;
        } catch (FileNotFoundException e) {
            Log.e("save file", e.getMessage());
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String loadFile(Context context, String fileName) {
        try {
            FileInputStream fis = context.openFileInput(fileName);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));
            String line = bufferedReader.readLine();
            bufferedReader.close();
            Log.i("loadFile", "File read successfully");
            return line;
        } catch (FileNotFoundException e) {
            Log.e("loadFile", e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e("loadFile", e.getMessage());
            return null;
        }
    }

    public void torchToggle(String command, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            CameraManager cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
            String cameraId;
            try {
                cameraId = cameraManager.getCameraIdList()[0];
                Log.i("Flashlight_onStart", "camera id " + cameraId);
                if (command.equals("on")) {
                    cameraManager.setTorchMode(cameraId, true);
                } else {
                    cameraManager.setTorchMode(cameraId, false);
                }
            } catch (CameraAccessException e) {
                Log.i("Flashlight_onStart", "exception " + e.getMessage());
            }
        }
    }
}
