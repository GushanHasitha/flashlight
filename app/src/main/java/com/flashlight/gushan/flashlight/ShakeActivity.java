package com.flashlight.gushan.flashlight;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class ShakeActivity extends Service {
    SensorManager sensorManager;
    private float shakeThreshold;
    private boolean isFlashlightOn = false;
    Global global;

    @Override
    public void onCreate() {
        super.onCreate();

        global = new Global();

        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        if (sensorManager != null) {
            Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(sensorEventListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        }

        String progress = Global.loadFile(getApplicationContext(), "settings.txt");
        Log.i("test", progress + "");
        if (progress != null) {
            shakeThreshold = Float.parseFloat(progress);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public final SensorEventListener sensorEventListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                float[] values = event.values;
                double acceleration = Math.sqrt(Math.pow(values[0], 2) +
                        Math.pow(values[1], 2) +
                        Math.pow(values[2], 2)) - SensorManager.GRAVITY_EARTH;

                if (acceleration > shakeThreshold && shakeThreshold > 8) {
                    Log.i("test", acceleration + "");
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    if (!isFlashlightOn) {
                        global.torchToggle("on", ShakeActivity.this);
                        intent.putExtra(Intent.EXTRA_TEXT, "Switch Off");
                        startActivity(intent);
                    } else {
                        global.torchToggle("off", ShakeActivity.this);
                        intent.putExtra(Intent.EXTRA_TEXT, "Switch On");
                        startActivity(intent);
                    }

                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };
}
